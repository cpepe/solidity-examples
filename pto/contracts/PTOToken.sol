// SPDX-License-Identifier: GPL-3.0
//implementation of a PTO system where 100 PTO is redemable 1hr of paid time off
//this implementation ignores our wellness program PTO rewards and comp PTO time given to employees
pragma solidity >=0.7.0 <0.9.0;

contract PTOToken {
    //owner has control over onboarding/offboarding, and minting new tokens. Owner is also the burn address
    address public owner;
    //token holders and their balances
    mapping(address => uint256) public balances;
    //track who is currently employed
    //this solves the issue of differentiating between Zero balance meaning NO PTO and UNKNOWN USER
    mapping(address => bool) public ptoEligible; //this could probably be private - employment status may be protected info?
    //100 tokens is equal to 1hr of paid time off
    //Employees accrue .22hrs/day ~= 80hrs/yr
    uint256 private _dailyQty = 22;
    uint256 private _maxBalance = 12000;

    constructor() {
        owner = msg.sender;
    }
    // modifier to check if caller is owner
    modifier isOwner() {
        // If the first argument of 'require' evaluates to 'false', execution terminates and all
        // changes to the state and to Ether balances are reverted.
        require(msg.sender == owner, "This function can only be called by the contract owner");
        _;
    }

    //ensure that wallet balance does not exceed 120
    function canReceive(address to, uint256 qty) private view {
        require( ptoEligible[to] == true, "User is unknown or inactive, cannot receive PTO"); //TODO: again, 0 is NO PTO and USER UNKNOWN - not sure how to solve this
        require( (balances[to] + qty) <= _maxBalance, "Balance to exceed max 120hrs - cannot complete transaction");
    }

    function onboard(address newUser, uint256 signingBonus) public isOwner {
        //if I read the docs correctly an uninitialized user will have a zero balance.
        //eligible array is an attempt to handle the case where a user has 0 PTO due to using it all up, not that they don't exist
        require( ptoEligible[newUser] == false, "User already has onboarded");  //don't override existing users
        //keep signingbonus bound to max allowed time off
        //here we assume that _maxBalance won't overflow uint256
        require( signingBonus <= _maxBalance, "Signing Bonus cannot exceed max PTO balance");
        //NOTE: returning employees loose their old balance when overwritten by signingBonus
        balances[newUser] = signingBonus;
        ptoEligible[newUser] = true;
    }

    function offboard(address outgoingUser) public isOwner{
        //NOTE: in this implementation there is no clear "pay out" of leftover PTO balance.
        //      Traditionally this is paid out - should allow for swap to some other currency
        //      or perhaps let them keep their PTO balance if it has real value
        //TODO: NOTE: it feels like this is the wrong way to move balances around
        balances[owner] += balances[outgoingUser]; //burn leftover PTO
        balances[outgoingUser] = 0;
        ptoEligible[outgoingUser] = false;
    }

    //add _dailyQty PTO to each wallet each day
    //Asumption here is this is called once per day by the contract controller
    /*
    It seems ill-advised to iterate over an unbounded array
    Contracts only execute transactions so no scheduler. Users could be billions so not wise to iterate over the entire mapping of addresses

    In the UI there should be logic to track all of the users in this system, and a schedule to handle airdropping
    PTO daily.
    */
    function mint(address to) public isOwner {
        //could refactor to iterate over pto_eligible and add to each user's wallet directly. That seems to be discouraged because the user count is not bound
        //  This logic forces tracking active employees, and minting schedule into the dApp and separates the minting frequency (in dApp), where the minting quantity is hardcoded here
        //  Could consider adding a throttle here to ensure minting only happens every 24hrs. Still the best solution is to set the minting schedule here but that doesn't seem possible.
        canReceive(to, _dailyQty); //throws an error if the caller's wallet is too full
        balances[to] += _dailyQty;
    }

    function takeTimeOff(address who, uint256 howLong) public {
        //for completeness - do we really care if an ineligable employee trys to take time off
        require( ptoEligible[who] == true);
        //ensure the user has enough PTO to take this time off
        //NOTE:this code never executes if too low of a balance - not sure why other than it knows the balance
        require( (balances[who] - howLong) >= 0, "Not enough PTO in wallet for the requested time off" );

        //burn the PTO tokens needed to buy this time off
        balances[who] -= howLong;
        balances[owner] += howLong;
    }

    function send(address to, uint256 qty) public {
        require(balances[to]+qty <= _maxBalance, "Receiving address cannot have over 12000 PTO");
        require(balances[msg.sender] >= qty, "Insufficient balance to send");
        //this still seems like a cheesy way to manage token balances
        balances[msg.sender] -= qty;
        balances[to] += qty;
    }
}
