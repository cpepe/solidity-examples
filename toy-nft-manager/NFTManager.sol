pragma solidity >=0.7.0 <0.9.0;

contract NFTSystemNameHere {
    address public owner;
    mapping(address => SuperValuableToken) public purses;

    // ensure caller is owner for some functions
    modifier isOwner() {
        require(msg.sender == owner, "Caller is not owner and is required to be");
        _;
    }

    struct SuperValuableToken {
        string value;
        string url;
        address[] owners;
    }

    //assign new item to address

    //transfer from address to address
    //ensure envoked by current owner
}
